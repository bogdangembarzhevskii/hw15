package data;

import models.Account;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.Date;
import java.util.List;

public class AccountDAO {

    private SessionFactory factory;

    public AccountDAO() {
        factory = new Configuration().configure
                ("hibernate.cfg.xml").addAnnotatedClass(Account.class).buildSessionFactory();
    }

    public void createAccount(Account account) {
        Session session = factory.getCurrentSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.save(account);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Account> getAllAccounts() {
        Session session = factory.getCurrentSession();
        Transaction transaction = null;
        List<Account> accounts = null;

        try {
            transaction = session.beginTransaction();
            accounts = session.createQuery
                    ("FROM models.Account WHERE deletedAt IS NULL", Account.class).getResultList();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return accounts;
    }

    public Account getAccountById(Long id) {
        Session session = factory.getCurrentSession();
        Transaction transaction = null;
        Account account = null;

        try {
            transaction = session.beginTransaction();
            account = session.createQuery
                            ("FROM models.Account WHERE id = :accountId AND deletedAt IS NULL", Account.class)
                    .setParameter("accountId", id)
                    .uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return account;
    }

    public void updateAccount(Account account) {
        Session session = factory.getCurrentSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            session.update(account);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void deleteAccount(Long id) {
        Session session = factory.getCurrentSession();
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            Account account = session.get(Account.class, id);
            account.setDeletedAt(new Date()); //Проставляет дату удаления. Сам аккаунт не удаляется
            session.update(account);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
    //Обновление GitLab.
}