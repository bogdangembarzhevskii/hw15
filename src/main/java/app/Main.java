package app;

import data.AccountDAO;
import models.Account;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        AccountDAO accountDAO = new AccountDAO();

        // Создание аккаунта
        Account newAccount = new Account();
        newAccount.setFirstName("Bogdan");
        newAccount.setLastName("Bogdanov");
        newAccount.setEmail("bogdan@gmail.com");
        accountDAO.createAccount(newAccount);

        // Вывод всех аккаунтов
        List<Account> allAccounts = accountDAO.getAllAccounts();
        System.out.println("Список аккаунтов: " + allAccounts);

        // Вытаскивает аккаунты из БД по id
        Long accountId = 1L;
        Account retrievedAccount = accountDAO.getAccountById(accountId);
        System.out.println("id: " + retrievedAccount);

        // Изменение существующего аккаунта
        retrievedAccount.setFirstName("userName");
        accountDAO.updateAccount(retrievedAccount);

        // Логическое удаление
        accountDAO.deleteAccount(accountId);

    }
    //Обновление GitLab.
}


